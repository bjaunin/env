#!/bin/bash
set -e

TIME=300

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
  echo + Stopping service 1

  kill $REDIS_PID_1 || true

  echo + Stopping service 2

  kill $REDIS_PID_2 || true

  echo + Stopping sentinel

  kill $REDIS_SENTINEL_PID || true
}

cat <<EOD > sentinel.dev.conf
sentinel monitor gitter-master 127.0.0.1 6379 1
sentinel down-after-milliseconds gitter-master 2000
sentinel failover-timeout gitter-master 5000
sentinel config-epoch gitter-master 5
EOD

rm -f dump.rdb

echo + Starting server 1

redis-server &
REDIS_PID_1=$!

echo + Starting server 2

redis-server --port 6380 --slaveof localhost 6379 &
REDIS_PID_2=$!

echo + Starting sentinel

redis-server ./sentinel.dev.conf --sentinel &
REDIS_SENTINEL_PID=$!

while true; do
  echo + Sleeping $TIME seconds to let the system settle

  sleep $TIME

  echo + Stopping server 1

  kill $REDIS_PID_1

  echo + Sleeping $TIME seconds to let the system settle

  sleep $TIME

  echo + Starting server 1

  redis-server &
  REDIS_PID_1=$!

  echo + Sleeping $TIME seconds to let the system settle

  sleep $TIME

  echo + Stopping server 2

  kill $REDIS_PID_2

  echo + Sleeping $TIME seconds to let the system settle

  sleep $TIME

  echo + Starting service 2

  redis-server --port 6380 &
  REDIS_PID_2=$!

done






