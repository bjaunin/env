"use strict";

var assert = require('assert');

describe('gitter-config', function() {
  it('should load the prod config correctly', function() {
    process.env.NODE_ENV = 'prod';
    var config = require('../lib/config').create(__dirname + '/config');

    assert.equal('prodcow', config.get('test:value'));
  });

  it('should load the dev config correctly', function() {
    process.env.NODE_ENV = 'dev';
    var config = require('../lib/config').create(__dirname + '/config');

    assert.equal('devcow', config.get('test:value'));
  });

  it('should load the default config correctly', function() {
    process.env.NODE_ENV = 'x';
    var config = require('../lib/config').create(__dirname + '/config');

    assert.equal('something', config.get('test:another_value'));
  });
});
