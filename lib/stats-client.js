/*jshint globalstrict:true, trailing:false, unused:true, node:true */
"use strict";

var processName = require('./process-name')

function emergencyLog() {
  console.error.apply(console, arguments);
}

exports.create = function(options) {
  var StatsD = require('node-statsd').StatsD;
  var statsdNodeJsVersionTags = require('./statsd-nodejs-version-tags');
  var path = require('path');

  // Stats requires a logger and logger requires stats, so
  // we'll forego logging in the stats module rather than the
  // other way around
  var config = options.config;
  var tags = options.tags;
  var prefix = options.prefix;

  var statsdEnabled = config.get("stats:statsd:enabled");

  var statsdClient;
  /**
   * statsd
   */
  if (statsdEnabled) {
    var globalTags = [];

    if (config.get("staging")) {
      globalTags.push("staging:1");
    } else {
      globalTags.push("staging:0");
    }

    if (tags) globalTags = globalTags.concat(tags);

    if (options.includeNodeVersionTags) {
      globalTags = globalTags.concat(statsdNodeJsVersionTags());
    }

    var appName = processName.getShortProcessName();
    if (appName) {
      globalTags.push('job:' + appName);
    }

    // Add the main entry point into the app as a tag
    if (require.main && require.main.filename) {
      var filename = require.main.filename;
      var entry = path.basename(filename, '.js');
      globalTags.push('main:' + entry);
    }

    statsdClient = new StatsD({
      prefix: prefix,
      host: '127.0.0.1',
      global_tags: globalTags
    });

    statsdClient.socket.on('error', function(error) {
      return emergencyLog("Error in statsd socket: " + error, { exception: error });
    });

  } else {
    // Just create a mock client
    statsdClient = new StatsD({ mock: true });
  }

  return statsdClient;
};
