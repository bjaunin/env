"use strict";

module.exports = function errorHandlerMiddleware(err, req, res, next) { // eslint-disable-line no-unused-vars
  var message = res.locals.errorMessage || "Internal Server Error";

  // TODO: Add an HTML version for HTML clients
  res.send({ error: message, uniqueId: res.locals.errorIdentifer });
};
