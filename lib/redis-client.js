"use strict";

var redis = require("redis");
var shutdown = require('shutdown');
var clients = [];
var _ = require('lodash');
var parseRedisOptions = require('./utils/redis-url-parser');
var debug = require('debug')('gitter:redis-client');
var Promise = require('bluebird');

/**
 * Switches redis database
 */
function switchDatabase(client, redisDb, logger) {
  if(redisDb) {
    debug('Switching redis database to #%s', redisDb);
    client.select(redisDb, function(err) {
      if(err) {
        logger.error('Unable to switch redis databases', err);
        /* This could be bad.. Need to consider a better way of handling it */
        throw err;
      }
    });
  }
}

/**
 * Creates a sentinel client
 */
function createSentinelClientInstance(sentinelOpts, clientOpts, logger) {
  var RedisSentinel = require('@gitterhq/redis-sentinel-client');

  /* redis-sentinel will always try redis sentinels from first to last
     so shuffle the list to ensure that no sentinel is slacking */
  var sentinels = _.shuffle(sentinelOpts.hosts).map(function(f) {
    var a = f.split(':');
    return [a[0], parseInt(a[1], 10)];
  });

  var sentinelClient = RedisSentinel.createClient({
    sentinels: sentinels,
    masterName: sentinelOpts["master-name"],
    masterOptions: clientOpts // Beware! Bit confusing this naming...
  });
  sentinelClient.setMaxListeners(50);

  if (debug.enabled) {
    ['sentinel connected', 'sentinel disconnected'].forEach(function(event) {
      sentinelClient.on(event, function() {
        debug('Redis sentinel client: %s', event);
      });
    });
  }

  ['failover start', 'failover end', 'switch master'].forEach(function(event) {
    sentinelClient.on(event, function() {
      logger.warn('Redis sentinel client: ' + event);
    });
  });

  sentinelClient.on('sentinel message', function(msg) {
    logger.info('Redis sentinel client: sentinel message: ' + msg);
  });

  sentinelClient.on('error', function(err) {
    logger.error('Redis error (node_redis sentinel): ' + err, { exception: err });
  });

  function validateConnection(client) {
    if (!client.host.match(new RegExp('^' + sentinelOpts.validateHost.replace(/\./g, '\\.').replace(/\*/g, '\\d+') + '$'))) {
      logger.error('**************************************');
      logger.error('* HERE BE DRAGONS                    *');
      logger.error('**************************************');
      logger.error('Host ' + client.host + ' does not match ' + sentinelOpts.validateHost + '.');
      logger.error('This is almost certainly not what you\'re looking to do');
      logger.error('**************************************');
    }
  }

  if (sentinelOpts.validateHost) {
    if (sentinelClient.activeMasterClient) {
      validateConnection(sentinelClient.activeMasterClient);
    } else {
      sentinelClient.once('reconnected', function() {
        validateConnection(sentinelClient.activeMasterClient);
      });
    }
  }

  return sentinelClient;
}

function createInstance(options, logger) {
  debug('Creating redis instance  with options %j', options);

  var client;
  if (typeof options === 'string') {
    options = parseRedisOptions(options);
  }

  if(options.sentinel) {
    client = createSentinelClientInstance(options.sentinel, options.clientOpts, logger);
  } else {
    var host = options.host;
    var port = options.port;

    client = redis.createClient(port, host, options.clientOpts);
    client.on('error', function(err) {
      logger.error('Redis error (node_redis): ' + err, { exception: err });
    });

  }
  switchDatabase(client, options.redisDb, logger);
  return client;
}

function registerClient(client) {
  clients.push(client);
}

function setupTransientClient(port, host, redisDb, clientOpts, logger) {
  var client = redis.createClient(port, host, clientOpts);
  client.on('error', function(err) {
    logger.error('Redis error (transient node_redis): ' + err, { exception: err });
  });
  switchDatabase(client, redisDb, logger);

  /*
   * Add some logging if the transient client is not closed
   */
  var timeout = setTimeout(function() {
    logger.warn('Transient redis connection has not been closed after 3 minutes.');
  }, 180000);

  client.once('end', function() {
    clearTimeout(timeout);
  });

  registerClient(client);
  return client;
}

exports.createTransientClient = function(mainClient, options, logger, callback) {
  var host, port;

  if (typeof options === 'string') {
    options = parseRedisOptions(options);
  }

  var redisDb = options.redisDb;
  var clientOpts = options.clientOpts;

  if(options.sentinel) {
    var master = mainClient.activeMasterClient;

    /* No master yet? Wait... */
    if(!master) {
      mainClient.on('reconnected', function() {
        var client = setupTransientClient(mainClient.activeMasterClient.port, mainClient.activeMasterClient.host, redisDb, clientOpts, logger);
        callback(null, client);
      });

      return;
    }

    host = master.host;
    port = master.port;
  } else {
    host = options.host;
    port = options.port;
  }

  var client = setupTransientClient(port, host, redisDb, clientOpts, logger);

  /* Callback in a second */
  setImmediate(function() {
    callback(null, client);
  });

};

shutdown.addHandler('redis', 1, function(callback) {
  return Promise.map(clients, function(client) {
    return Promise.fromCallback(function(callback) {
      // Workaround for bug in redis-sentinel client
      // until this patched
      // Callback called multiple times on quit
      var callbackCount = client.activeMasterClient ? 3 : 1;

      quit(client, function() {
        callbackCount--;
        if(callbackCount === 0) callback();
      });
    })
  })
  .asCallback(callback);
});

exports.create = function(options, logger) {
  var client = createInstance(options, logger);
  registerClient(client);

  return client;
};

function quit(client, callback) {

  for(var i = 0; i < clients.length; i++) {
    if(clients[i] === client) {
      clients.splice(i, 1);
      break;
    }
  }

  client.quit(callback);
}
exports.quit = quit;


exports.testOnly = {
  getClients: function() {
    return clients;
  }
};
