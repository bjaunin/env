/*jshint globalstrict:true, trailing:false, unused:true, node:true */
"use strict";

exports.create = function(configDirectory) {
  var config = require('./config').create(configDirectory);

  var logger = require('./logger').create({ config: config });
  var stats = require('./stats').create({ config: config, logger: logger });
  var errorReporter = require('./error-reporter').create({ config: config, logger: logger });
  var redisClient = require('./redis-client');
  var errorHandlerMiddleware = require('./middlewares/error-handler');
  var errorReporterMiddleware = require('./middlewares/error-reporter');
  var accessLogger = require('./middlewares/access-logger');
  var identifyRouteMiddleware = require('./middlewares/identify-route');
  var unhandledExceptionHandler = require('./unhandled-exceptions');
  var redisUrlParser = require('./utils/redis-url-parser');

  /* Monitor event loop blockages */
  require('./block-monitor').create({ config: config, stats: stats, logger: logger });

  /* Main singleton client */
  var mainRedisClient;

  var middlewares = {
    identifyRoute: identifyRouteMiddleware
  };

  function createErrorReporterMiddleware() {
    var statsPrefix = config.get('stats:statsd:prefix');

    var statsClient = require('./stats-client').create({
      config: config,
      prefix: statsPrefix,
      includeNodeVersionTags: true
    });
    return errorReporterMiddleware.create({ config: config, logger: logger, statsClient: statsClient, errorReporter: errorReporter});
  }

  Object.defineProperty(middlewares, 'errorHandler', {
    enumerable: true,
    get: function() {
      var errorReporterMiddleware = createErrorReporterMiddleware();
      return function(err, req, res, next) {
        /* Invoke the error reporter before calling the handler */
        errorReporterMiddleware(err, req, res, function(err) {
          return errorHandlerMiddleware(err, req, res, next);
        });
      };
    }
  });

  Object.defineProperty(middlewares, 'errorReporter', {
    enumerable: true,
    get: createErrorReporterMiddleware
  });

  Object.defineProperty(middlewares, 'accessLogger', {
    enumerable: true,
    get: function() {
      return accessLogger.create({
        config: config
      });
    }
  });

  var env = {
    config: config,
    logger: logger,
    errorReporter: errorReporter,
    stats: stats,
    createStatsClient: function createStatsClient(options) {
      return require('./stats-client').create({
        config: config,
        prefix: options && options.prefix,
        tags: options && options.tags,
        includeNodeVersionTags: options && options.includeNodeVersionTags,
       });
    },

    installUncaughtExceptionHandler: function() {
      process.on('uncaughtException', unhandledExceptionHandler.create({ config: config, logger: logger, errorReporter: errorReporter }));
    },

    domainWrap: function(inside) {
      /* Only require this where we use it as requiring domains changes the way node works */
      var domain = require('domain');

      // create a top-level domain for the server
      var serverDomain = domain.create();
      serverDomain.on('error', unhandledExceptionHandler.create({ config: config, logger: logger, errorReporter: errorReporter }));
      serverDomain.run(inside);
    },

    installDiagnosticTools: function() {
      require('./install-diagnostics').install({ logger: logger });
    },

    redis: {
      getClient: function() {
        if(mainRedisClient) return mainRedisClient;

        // no options override for singleton
        var options = process.env.REDIS_CONNECTION_STRING || config.get('redis');
        mainRedisClient = redisClient.create(options, logger);

        return mainRedisClient;
      },

      createClient: function(options) {
        options = options || process.env.REDIS_CONNECTION_STRING || config.get('redis');
        return redisClient.create(options, logger);
      },

      /**
       * Returns a short-lived connection, which will not be immune to a redis failover
       */
      createTransientClient: function(callback) {
        var mainClient = this.getClient();

        // no options override for singleton
        var options = process.env.REDIS_CONNECTION_STRING || config.get('redis');
        return redisClient.createTransientClient(mainClient, options, logger, callback);
      },

      quitClient: function(client) {
        return redisClient.quit(client);
      },

      parse: function(connectionString) {
        return redisUrlParser(connectionString);
      }
    },

    ioredis: require('./env-ioredis').create({ config: config, logger: logger }),

    mongo: {
      /* Bring your own mongoose connection configurer */
      configureMongoose: function(mongoose) {
        return require('./mongoose-connection-configurer')({ config: config, logger: logger, mongoose: mongoose, env: env });
      }
    },

    middlewares: middlewares
  };

  /* Lazy load mailer */
  var mailer;
  Object.defineProperty(env, 'mailer', {
    enumerable: true,
    get: function() {
      if (mailer) return mailer;
      mailer = require('./mailer').create({ logger: logger, stats: stats, config: config });
      return mailer;
    }
  });

  return env;
};
